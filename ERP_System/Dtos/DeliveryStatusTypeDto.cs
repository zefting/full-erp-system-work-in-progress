﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class DeliveryStatusTypeDto
    {
        public DeliveryStatusTypeDto()
        {
            SalesDeliveredsDto = new HashSet<SalesDeliveredDto>();
        }

        public int DeliveryStatusTypeId { get; set; }
        public string DeliveryTypeName { get; set; }
        public string DeliveryTypeDescription { get; set; }

        public virtual ICollection<SalesDeliveredDto> SalesDeliveredsDto { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using ERP_System.Models;
#nullable disable

namespace ERP_System.Dtos
{
    public partial class CountryWithRegionDto
    {
       

        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        
       
    }
}

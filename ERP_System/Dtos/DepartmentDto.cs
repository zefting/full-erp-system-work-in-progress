﻿using ERP_System.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class DepartmentDto
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int? LocationId { get; set; }

        public virtual LocationDto LocationDto { get; set; }

        
    }
}

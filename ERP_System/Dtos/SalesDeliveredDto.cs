﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class SalesDeliveredDto
    {
        public int SalesDeliveredId { get; set; }
        public int? SalesId { get; set; }
        public int? DeliveryStatusTypeId { get; set; }

        public virtual DeliveryStatusTypeDto DeliveryStatusTypeDto { get; set; }
        public virtual SaleDto SalesDto { get; set; }
    }
}

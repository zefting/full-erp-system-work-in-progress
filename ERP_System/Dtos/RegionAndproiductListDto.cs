﻿using ERP_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP_System.Dtos
{
    public class RegionAndProductListDto
    {
        public List<ProductSale> productSaleList { get; set; }
        public List<RegionProduct> regionProductList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class ProductSaleDto
    {
        public int Psid { get; set; }
        public int ProductId { get; set; }
        public int SalesId { get; set; }

        public virtual ProductDto Product { get; set; }
        public virtual SaleDto Sales { get; set; }
    }
}

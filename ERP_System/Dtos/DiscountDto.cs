﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class DiscountDto
    {
        public int DiscountId { get; set; }
        public int? Account { get; set; }
        public string Discription { get; set; }
        public double? Amount { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int? SalesId { get; set; }

        public virtual AccountDto AccountDtoNavigation { get; set; }
        public virtual SaleDto SalesDto { get; set; }
    }
}

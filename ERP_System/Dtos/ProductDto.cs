﻿using ERP_System.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class ProductDto
    {
        public ProductDto()
        {
            RegionProductsDto = new HashSet<RegionProductDto>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int? CategoryId { get; set; }

        public virtual CategoryDto CategoryDto { get; set; }
        public virtual ICollection<RegionProductDto> RegionProductsDto { get; set; }

        
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class AccountDto
    {
        public AccountDto()
        {
            CostOfGoodsSoldsDto = new HashSet<CostOfGoodsSoldDto>();
            DiscountsDto = new HashSet<DiscountDto>();
            ExpensesDto = new HashSet<ExpenseDto>();
        }

        public int AccountId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CostOfGoodsSoldDto> CostOfGoodsSoldsDto { get; set; }
        public virtual ICollection<DiscountDto> DiscountsDto { get; set; }
        public virtual ICollection<ExpenseDto> ExpensesDto { get; set; }
    }
}

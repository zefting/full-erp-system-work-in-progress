﻿using ERP_System.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class JobDto
    {
        public JobDto()
        {
            EmployeesDto = new HashSet<EmployeeDto>();
        }

        public int JobId { get; set; }
        public string JobTitle { get; set; }

        public virtual ICollection<EmployeeDto> EmployeesDto { get; set; }

        
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class InvoiceDto
    {
        public int InvoicesId { get; set; }
        public int? SalesId { get; set; }
        public int? InvoicePaidId { get; set; }

        public virtual InvoicePaidDto InvoicePaidDto { get; set; }
        public virtual SaleDto SalesDto { get; set; }
    }
}

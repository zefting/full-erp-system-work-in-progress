﻿using ERP_System.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class EmployeeDto
    {
        public EmployeeDto()
        {
            DependentsDto = new HashSet<DependentDto>();
        }

        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
        public DateTime HireDate { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public int? JobId { get; set; }
        public int? DepartmentId { get; set; }

        public virtual DepartmentDto DepartmentDto { get; set; }
        public virtual JobDto JobDto { get; set; }
        public virtual ICollection<DependentDto> DependentsDto { get; set; }

        
    }
}

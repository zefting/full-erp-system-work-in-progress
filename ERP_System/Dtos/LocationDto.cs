﻿using ERP_System.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class LocationDto
    {
        public LocationDto()
        {
            DepartmentsDto = new HashSet<DepartmentDto>();
        }

        public int LocationId { get; set; }
        public string StreetAddress { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public int? CountryId { get; set; }

        public virtual CountryDto CountryDto { get; set; }
        public virtual ICollection<DepartmentDto> DepartmentsDto { get; set; }

        
    }
}

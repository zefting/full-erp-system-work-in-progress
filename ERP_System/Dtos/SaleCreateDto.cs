﻿using System;
using System.Collections.Generic;
using ERP_System.Models;
#nullable disable

namespace ERP_System.Dtos
{
    public partial class SaleCreateDto
    {
        public SaleCreateDto()
        {
            CostOfGoodsSoldsDto = new HashSet<CostOfGoodsSoldDto>();
            DiscountsDto = new HashSet<DiscountDto>();
            ExpensesDto = new HashSet<ExpenseDto>();
            InvoicesDto = new HashSet<InvoiceDto>();
            ProductSaleDto = new HashSet<ProductSaleDto>();
            SalesDeliveredsDto = new HashSet<SalesDeliveredDto>();
        }

       
        public int? CustomerId { get; set; }
        public List<int?> ProductsIds { get; set; } 
        public int RegionId { get; set; }
        public DateTime? TransactionDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<CostOfGoodsSoldDto> CostOfGoodsSoldsDto { get; set; }
        public virtual ICollection<DiscountDto> DiscountsDto { get; set; }
        public virtual ICollection<ExpenseDto> ExpensesDto { get; set; }
        public virtual ICollection<InvoiceDto> InvoicesDto { get; set; }
        public virtual ICollection<ProductSaleDto> ProductSaleDto { get; set; }
        public virtual ICollection<SalesDeliveredDto> SalesDeliveredsDto { get; set; }

       
    }
}

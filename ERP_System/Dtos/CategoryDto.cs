﻿using ERP_System.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class CategoryDto
    {
        public CategoryDto()
        {
            ProductsDto = new HashSet<ProductDto>();
        }

        public int CategoryId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProductDto> ProductsDto { get; set; }

        
    }
}

﻿using System;
using System.Collections.Generic;
using ERP_System.Models;
#nullable disable

namespace ERP_System.Dtos
{
    public partial class CustomerReadDto
    {
        public CustomerReadDto()
        {
            SalesDto = new HashSet<SaleDto>();
        }

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int? CountryId { get; set; }
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }

        public virtual CountryDto CountryDto { get; set; }
        public virtual ICollection<SaleDto> SalesDto { get; set; }

       
    }
}

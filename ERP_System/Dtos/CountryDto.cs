﻿using System;
using System.Collections.Generic;
using ERP_System.Models;
#nullable disable

namespace ERP_System.Dtos
{
    public partial class CountryDto
    {
        public CountryDto()
        {
            CustomersDto = new HashSet<CustomerReadDto>();
            LocationsDto = new HashSet<LocationDto>();
        }

        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int? RegionId { get; set; }

        public virtual RegionDto RegionDto { get; set; }
        public virtual ICollection<CustomerReadDto> CustomersDto { get; set; }
        public virtual ICollection<LocationDto> LocationsDto { get; set; }

        
    }
}

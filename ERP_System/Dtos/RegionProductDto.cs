﻿using ERP_System.Models;
using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class RegionProductDto
    {
        public RegionProductDto()
        {
            SalesDto = new HashSet<SaleDto>();
        }

        public int RegionProductsId { get; set; }
        public int? ProductId { get; set; }
        public int? RegionId { get; set; }

        public virtual ProductDto ProductDto { get; set; }
        public virtual RegionDto RegionDto { get; set; }
        public virtual ICollection<SaleDto> SalesDto { get; set; }

        
    }
}

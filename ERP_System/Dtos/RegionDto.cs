﻿using System;
using System.Collections.Generic;
using ERP_System.Models;
#nullable disable

namespace ERP_System.Dtos
{
    public partial class RegionDto
    {
       
        public int RegionId { get; set; }
        public string RegionName { get; set; }

        public virtual ICollection<CountryDto> CountriesDto { get; set; }
        public virtual ICollection<RegionProductDto> RegionProductsDto { get; set; }

        
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class InvoicePaidDto
    {
        public InvoicePaidDto()
        {
            InvoicesDto = new HashSet<InvoiceDto>();
        }

        public int InvoicePaidId { get; set; }
        public DateTime? InvoiceFinalDate { get; set; }
        public bool? InvoicePaid1 { get; set; }

        public virtual ICollection<InvoiceDto> InvoicesDto { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using ERP_System.Models;
#nullable disable

namespace ERP_System.Dtos
{
    public partial class SaleDto
    {
        public SaleDto()
        {
            CostOfGoodsSolds = new HashSet<CostOfGoodsSold>();
            Discounts = new HashSet<Discount>();
            Expenses = new HashSet<Expense>();
            Invoices = new HashSet<Invoice>();
            ProductSales = new HashSet<ProductSale>();
            SalesDelivereds = new HashSet<SalesDelivered>();
        }

        public int SalesId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? TransactionDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<CostOfGoodsSold> CostOfGoodsSolds { get; set; }
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Expense> Expenses { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<ProductSale> ProductSales { get; set; }
        public virtual ICollection<SalesDelivered> SalesDelivereds { get; set; }
        }
    }


﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Dtos
{
    public partial class ExpensesTypeDto
    {
        public ExpensesTypeDto()
        {
            ExpensesDto = new HashSet<ExpenseDto>();
        }

        public int ExpensesTypeId { get; set; }
        public string Discription { get; set; }

        public virtual ICollection<ExpenseDto> ExpensesDto { get; set; }
    }
}

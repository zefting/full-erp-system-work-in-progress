﻿using ERP_System.Data;
using ERP_System.Dtos;
using ERP_System.Models;
using System.Collections.Generic;

namespace ERP_System.Services
{
    public class MapperService : IMapperService
    {
        private readonly IERP_SystemRepo _repository;

        public MapperService(IERP_SystemRepo repository)
        {
            _repository = repository;
        }

        public CategoryDto MapToCategoryDto(Category category)
        {
            return new CategoryDto
            {
                CategoryId = category.CategoryId,
                Name = category.Name
            };
        }

        public RegionDto MapToRegionDto(Region region)
        {
            return new RegionDto
            {
                RegionId = region.RegionId,
                RegionName = region.RegionName
            };
        }

        public CountryDto MapToCountryDto(Country country)
        {
            return new CountryDto
            {
                CountryId = country.CountryId,
                CountryName = country.CountryName,
                RegionDto = MapToRegionDto(country.Region)
            };
        }

        public ProductDto MapToProductDto(Product product)
        {
            return new ProductDto
            {
                ProductId = product.ProductId,
                ProductName = product.ProductName,
                CategoryId = product.CategoryId,
                CategoryDto = MapToCategoryDto(product.Category)
            };
        }

        public RegionProductDto MapToRegionProductDto(RegionProduct regionProduct)
        {
            return new RegionProductDto
            {
                RegionProductsId = regionProduct.RegionProductsId,
                ProductDto = MapToProductDto(regionProduct.Product),
                ProductId = regionProduct.ProductId,
                RegionId = regionProduct.RegionId,
                RegionDto = MapToRegionDto(regionProduct.Region)
            };
        }

        public Customer customerToDb(CustomerCreateDto customerCreateDto)
        {
            return new Customer
            {
                Address = customerCreateDto.Address,
                City = customerCreateDto.City,
                CountryId = customerCreateDto.CountryId,
                Email = customerCreateDto.Email,
                FirstName = customerCreateDto.FirstName,
                LastName = customerCreateDto.LastName,
                PhoneNumber = customerCreateDto.PhoneNumber,
                PostalCode = customerCreateDto.PostalCode,
                StateProvince = customerCreateDto.StateProvince
            };
        }

        public CustomerReadDto MapToCustomerDto(Customer customer)
        {
            return new CustomerReadDto
            {
                Address = customer.Address,
                City = customer.City,
                CustomerId = customer.CustomerId,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PhoneNumber = customer.PhoneNumber,
                PostalCode = customer.PostalCode,
                StateProvince = customer.StateProvince,
                CountryId = customer.CountryId,
                CountryDto = MapToCountryDto(customer.Country)
            }
        }

        public LocationDto MapToLocationDto(Location location)
        {
            return new LocationDto
            {
                LocationId = location.LocationId,
                StreetAddress = location.StreetAddress,
                PostalCode = location.PostalCode,
                City = location.City,
                StateProvince = location.StateProvince,
                CountryId = location.CountryId,
                CountryDto = MapToCountryDto(location.Country)
            };
        }

        public DepartmentDto MapToDepartmentDto(Department department)
        {
            return new DepartmentDto
            {
                DepartmentName = department.DepartmentName,
                DepartmentId = department.DepartmentId,
                LocationId = department.LocationId,
                LocationDto = MapToLocationDto(department.Location)
            };
        }

        public Sale SaleToDb(SaleCreateDto saleDto)
        {
            return new Sale
            {
                CustomerId = saleDto.CustomerId,
                TransactionDate = saleDto.TransactionDate
            };
        }

        public SaleDto MapToSalesDto(Sale sale)
        {
            return new SaleDto
            {
                SalesId = sale.SalesId,
                CustomerId = sale.SalesId,
                TransactionDate = sale.TransactionDate
            };
        }

        public JobDto MapToJobDto(Job job)
        {
            return new JobDto
            {
                JobId = job.JobId,
                JobTitle = job.JobTitle
            };
        }

        public EmployeeDto MapToEmployeeDto(Employee employee)
        {
            return new EmployeeDto
            {
                City = employee.City,
                DepartmentDto = MapToDepartmentDto(employee.Department),
                DepartmentId = employee.DepartmentId,
                Email = employee.Email,
                EmployeeId = employee.EmployeeId,
                FirstName = employee.FirstName,
                HireDate = employee.HireDate,
                JobDto = MapToJobDto(employee.Job),
                JobId = employee.JobId,
                LastName = employee.LastName,
                PhoneNumber = employee.PhoneNumber,
                PostalCode = employee.PostalCode,
                StateProvince = employee.StateProvince
            };
        }

        public Employee employeeToDb(EmployeeDto employeeDto)
        {
            return new Employee
            {
                City = employeeDto.City,
                DepartmentId = employeeDto.DepartmentId,
                Email = employeeDto.Email,
                FirstName = employeeDto.FirstName,
                HireDate = employeeDto.HireDate,
                JobId = employeeDto.JobId,
                LastName = employeeDto.LastName,
                PhoneNumber = employeeDto.PhoneNumber,
                PostalCode = employeeDto.PostalCode,
                StateProvince = employeeDto.StateProvince
            };
        }

        public RegionAndProductListDto MapToregionAndProductListDto(SaleCreateDto saleCreateDto, int saleId)
        {
            List<ProductSale> productSaleList = new();
            List<RegionProduct> regionProductList = new();
            foreach (var item in saleCreateDto.ProductsIds)
            {
                ProductSale productSale = new ProductSale
                {
                    ProductId = (int)item,
                    SalesId = saleId
                };
                RegionProduct regionProductToList = new RegionProduct
                {
                    ProductId = item,
                    RegionId = saleCreateDto.RegionId
                };
                regionProductList.Add(regionProductToList);
                productSaleList.Add(productSale);
            }
            return new RegionAndProductListDto { productSaleList = productSaleList, regionProductList = regionProductList };
        }
    }
}
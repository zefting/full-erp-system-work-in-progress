import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { CustomerDataService } from '../customer-data.service';
import { SearchCustomerService } from '../search-customer.service';

@Component({
  selector: 'app-search-customers',
  templateUrl: './search-customers.component.html',
  styleUrls: ['./search-customers.component.css']
  
})
export class SearchCustomersComponent implements OnInit {
  public model: any;
 
  selectedCustomers: any;
  dropdownSettings = {};
    
  
  constructor(private customerDataService: CustomerDataService, private searchCustomerService: SearchCustomerService) { }
  allCustomers: any = [];
  customerFound: any;
  
  ngOnInit(): void {
   
    this.customerDataService.SendGetAllCustomerRequest().subscribe(data => {
      console.log(data);
      this.allCustomers = data;
    })

    this.selectedCustomers = [];
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'customerId',
      textField: 'firstName' ,     
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }
 
  announce() {
    
    this.searchCustomerService.AnnounceCustomerSelected(this.customerFound);
  }
  onItemSelect(item: any) {
    console.log(item);
    this.customerFound = this.allCustomers.filter(item => item.id === this.selectedCustomers.id)
    console.log(this.customerFound);
    
    console.log("called - model" + this.selectedCustomers)
    
    
  }
  
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewestCustomerComponent } from './newest-customer.component';

describe('NewestCustomerComponent', () => {
  let component: NewestCustomerComponent;
  let fixture: ComponentFixture<NewestCustomerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewestCustomerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewestCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

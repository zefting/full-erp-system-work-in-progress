import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-salesbycountryregion',
  templateUrl: './salesbycountryregion.component.html',
  styleUrls: ['./salesbycountryregion.component.css']
})
export class SalesbycountryregionComponent implements OnInit {

  constructor() { }
  public salesIsCollapsed = true;
  public regionIsCollapsed = true;
  ngOnInit(): void {
  }

}

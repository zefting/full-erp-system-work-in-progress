import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesbycountryregionComponent } from './salesbycountryregion.component';

describe('SalesbycountryregionComponent', () => {
  let component: SalesbycountryregionComponent;
  let fixture: ComponentFixture<SalesbycountryregionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesbycountryregionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesbycountryregionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

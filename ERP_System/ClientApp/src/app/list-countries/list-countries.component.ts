import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IDropdownSettings} from 'ng-multiselect-dropdown';

import { CountriesService } from '../countries.service';

@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styleUrls: ['./list-countries.component.css']
})
export class ListCountriesComponent implements OnInit {
  
  public model: any;
  dropdownList = [];
  selectedItems:any = [];
  dropdownSettings = {};
  @Output() CountrySelected = new EventEmitter<any>();
  constructor(private countriesService: CountriesService) { }
  contriesWithRegions:any = []
  ngOnInit(): void {
    this.countriesService.SendGetAllCountriesWithRegionsRequest().subscribe(data => {
      console.log(data);
      this.contriesWithRegions = data;
    })


   this.selectedItems = [
];
this.dropdownSettings = {
  singleSelection: false,
  idField: 'countryId',
  textField: 'countryName',
  selectAllText: 'Select All',
  unSelectAllText: 'UnSelect All',
  itemsShowLimit: 3,
  allowSearchFilter: true
};
  }
onItemSelect(item: any) {
  console.log(item);
}
onSelectAll(items: any) {
  console.log(items);
}

}

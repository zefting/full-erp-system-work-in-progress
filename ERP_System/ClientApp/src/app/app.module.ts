import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AllCustomersComponent } from "./all-customers/all-customers.component";
import { SearchCustomersComponent } from "./search-customers/search-customers.component";
import { ListCountriesComponent  } from "./list-countries/list-countries.component";
import { CommonModule } from '@angular/common';
import { SearchCustomerService } from './search-customer.service';
import { SalesbycountryregionComponent } from './salesbycountryregion/salesbycountryregion.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
  
    DashboardComponent,
    AllCustomersComponent,
    SearchCustomersComponent,
    ListCountriesComponent,
    SalesbycountryregionComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    CommonModule,
    NgMultiSelectDropDownModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: DashboardComponent, pathMatch: 'full' }
     
    ]),
    NgbModule
  ],
  providers: [SearchCustomerService],
  bootstrap: [AppComponent]
})
export class AppModule { }

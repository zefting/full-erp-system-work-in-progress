import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPrRegionComponent } from './sales-pr-region.component';

describe('SalesPrRegionComponent', () => {
  let component: SalesPrRegionComponent;
  let fixture: ComponentFixture<SalesPrRegionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesPrRegionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPrRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

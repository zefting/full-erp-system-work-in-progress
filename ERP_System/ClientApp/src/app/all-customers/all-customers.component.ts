import { Component,Input,OnChanges,OnDestroy } from '@angular/core';
import { CustomerDataService } from '../customer-data.service';
import { SearchCustomerService } from '../search-customer.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-all-customers',
  templateUrl: './all-customers.component.html',
  styleUrls: ['./all-customers.component.css']
})
export class AllCustomersComponent implements OnDestroy{
  subscription: Subscription;
  customers: any = []
  constructor(private customerDataService: CustomerDataService, private searchCustomerService: SearchCustomerService) {
    this.subscription = this.searchCustomerService.customerSelected$.subscribe(customers => this.customers =customers);
   
  }
 
    
  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
  }
 
  
 
  
}

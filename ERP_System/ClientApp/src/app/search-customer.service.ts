import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
interface ICustomer 
{
  customerId: number;
  firstName: string;
  lastName: string;
  address: string;
  countryId: number;
  city: string;
  countryDto: object;
  email: string;
  phoneNumber: number;
  postalCode: number;
  salesDto: object;
  stateProvince: string;
}

//https://angular.io/guide/component-interaction
@Injectable({
  providedIn: 'root'
})
export class SearchCustomerService {
  // Observable string sources
  private customerSelectedSource = new Subject<ICustomer>();
  // Observable string streams
  customerSelected$ = this.customerSelectedSource.asObservable();
  // Service message commands
  AnnounceCustomerSelected(customer: ICustomer) {
    this.customerSelectedSource.next(customer);
    console.log("Service: ",customer);
    
  }
  constructor() { }
}

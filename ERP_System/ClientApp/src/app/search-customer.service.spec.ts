import { TestBed } from '@angular/core/testing';

import { SearchCustomerService } from './search-customer.service';

describe('SearchCustomerService', () => {
  let service: SearchCustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchCustomerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP_System.Data;
using ERP_System.Dtos;
using ERP_System.Models;
using ERP_System.Services;

namespace ERP_System.Controllers
{
   
    [Route("api/employee")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IMapperService _mapperService;
        private readonly IERP_SystemRepo _repository;

        public EmployeesController(IERP_SystemRepo repository, IMapperService mapperService)
        {
            _repository = repository;
            _mapperService = mapperService;
        }
       
        [HttpGet]
        public ActionResult<IEnumerable<EmployeeDto>> GetEmployees()
        {
            var employeesFromRepo = _repository.GetEmployees();
            var employeesDtoList = new List<EmployeeDto>();
            foreach (var employeeFromR in employeesFromRepo)
            {
                employeesDtoList.Add(_mapperService.MapToEmployeeDto(employeeFromR));
            }
            return employeesDtoList;
        }
        [HttpGet("{employee_id}", Name ="getEmployeeById")]
        public ActionResult<EmployeeDto> getEmployeeById(int employee_id)
        {
            var employeeFromRepo = _repository.GetEmployeeById(employee_id);
            if (employeeFromRepo !=null)
            {
                return Ok(_mapperService.MapToEmployeeDto(employeeFromRepo));
            }
            return NotFound();
        }
        [HttpPost]
        public ActionResult<EmployeeDto> CreateEmployee(EmployeeDto employeeDto)
        {
            Employee employeeModel = _mapperService.employeeToDb(employeeDto);
            _repository.CreateEmployee(employeeModel);
            _repository.SaveChanges();
            return _mapperService.MapToEmployeeDto(employeeModel);
        }
    }
}

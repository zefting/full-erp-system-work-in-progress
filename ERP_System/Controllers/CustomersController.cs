﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP_System.Data;
using ERP_System.Models;
using ERP_System.Dtos;
using ERP_System.Services;
namespace ERP_System.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IMapperService _mapperService;
        private readonly IERP_SystemRepo _repository;
        public CustomersController(IERP_SystemRepo repository, IMapperService mapperService)
        {
            _repository = repository;
            _mapperService = mapperService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CustomerReadDto>> GetAllCustomers()
        {
            var customersList = _repository.GetCustomers();
            
            if (customersList != null)
            {
                List<CustomerReadDto> customerReadDtoList = new();
                foreach (var customer in customersList)
                {
                    customerReadDtoList.Add(_mapperService.MapToCustomerDto(customer));
                }
                return Ok(customerReadDtoList);
            }
            else
            {
                return NotFound();
            }
           
            
        }

        [HttpGet("customer_id", Name = "GetCustomerById")]
        public ActionResult<CustomerReadDto> GetCustomerById(int customer_id)
        {
            Customer customerByIdFromRepo = _repository.GetCustomerById(customer_id);
            if (customerByIdFromRepo != null)
            {
                return Ok(_mapperService.MapToCustomerDto(customerByIdFromRepo));
            }
            else
            {
                return NotFound();
            }
        }
        [HttpPost]
        public ActionResult<CustomerReadDto> CreateNewCustomer(CustomerCreateDto customerCreateDto)
        {
            Customer customerModel = _mapperService.customerToDb(customerCreateDto);
            _repository.CreateCustomer(customerModel);
            _repository.SaveChanges();
            return _mapperService.MapToCustomerDto(customerModel);
        }
    }
}

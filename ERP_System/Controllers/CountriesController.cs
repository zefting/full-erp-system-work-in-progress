﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP_System.Data;
using ERP_System.Dtos;
using ERP_System.Models;
namespace ERP_System.Controllers
{
    [Route("api/countries")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly IERP_SystemRepo _repository;


        public CountriesController(IERP_SystemRepo repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CountryWithRegionDto>> GetAllCountiesWithRegions()
        {
            return _repository.GetAllCountryWithRegion().ToList();
        }
    }
}

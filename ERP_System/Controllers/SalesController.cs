﻿using ERP_System.Data;
using ERP_System.Dtos;
using ERP_System.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ERP_System.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly IERP_SystemRepo _repository;
        private readonly IMapperService _mapperService;

        public SalesController(IERP_SystemRepo repository, IMapperService mapperService)
        {
            _repository = repository;
            _mapperService = mapperService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SaleDto>> GetSales()
        {
            var salesItems = _repository.GetSales();
            var salesDtos = new List<SaleDto>();
            foreach (var sale in salesItems)
            {
                salesDtos.Add(_mapperService.MapToSalesDto(sale));
            }
            return salesDtos;
        }


        [HttpPost]
        public ActionResult<Sale> CreateSale(SaleCreateDto saleCreateDto)
        {
            Sale saleModel = _mapperService.SaleToDb(saleCreateDto);
            _repository.CreateSale(saleModel);
            _repository.SaveChanges();

            var saleregtionandproduct = _mapperService.MapToregionAndProductListDto(saleCreateDto, saleModel.SalesId);
            // save the products sold in that region
            _repository.SaveRegionProductsforSale(saleregtionandproduct.regionProductList);
            _repository.SaveChanges();
            // save products for the sale
            _repository.SaveSaleProducts(saleregtionandproduct.productSaleList);
            _repository.SaveChanges();

            return saleModel;
        }
    }
}
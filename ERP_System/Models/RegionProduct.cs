﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class RegionProduct
    {
        public int RegionProductsId { get; set; }
        public int? ProductId { get; set; }
        public int? RegionId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Region Region { get; set; }
    }
}

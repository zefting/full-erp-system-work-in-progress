﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class DeliveryStatusType
    {
        public DeliveryStatusType()
        {
            SalesDelivereds = new HashSet<SalesDelivered>();
        }

        public int DeliveryStatusTypeId { get; set; }
        public string DeliveryTypeName { get; set; }
        public string DeliveryTypeDescription { get; set; }

        public virtual ICollection<SalesDelivered> SalesDelivereds { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class ExpensesType
    {
        public ExpensesType()
        {
            Expenses = new HashSet<Expense>();
        }

        public int ExpensesTypeId { get; set; }
        public string Discription { get; set; }

        public virtual ICollection<Expense> Expenses { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class SalesDelivered
    {
        public int SalesDeliveredId { get; set; }
        public int? SalesId { get; set; }
        public int? DeliveryStatusTypeId { get; set; }

        public virtual DeliveryStatusType DeliveryStatusType { get; set; }
        public virtual Sale Sales { get; set; }
    }
}

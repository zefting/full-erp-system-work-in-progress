﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ERP_System.Models
{
    public partial class ERP_systemContext : DbContext
    {
        public ERP_systemContext()
        {
        }

        public ERP_systemContext(DbContextOptions<ERP_systemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CostOfGoodsSold> CostOfGoodsSolds { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DeliveryStatusType> DeliveryStatusTypes { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Dependent> Dependents { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Expense> Expenses { get; set; }
        public virtual DbSet<ExpensesType> ExpensesTypes { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoicePaid> InvoicePaids { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductSale> ProductSales { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<RegionProduct> RegionProducts { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<SalesDelivered> SalesDelivereds { get; set; }

       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Danish_Norwegian_CI_AS");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("accounts");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("description");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("categories");

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<CostOfGoodsSold>(entity =>
            {
                entity.HasKey(e => e.CostOfGoodsSoldsId)
                    .HasName("PK__cost_of___695969F6E055F531");

                entity.ToTable("cost_of_goods_solds");

                entity.Property(e => e.CostOfGoodsSoldsId).HasColumnName("cost_of_goods_solds_id");

                entity.Property(e => e.Account).HasColumnName("account");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Discription)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("discription");

                entity.Property(e => e.SalesId).HasColumnName("sales_id");

                entity.Property(e => e.TransactionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("transaction_date");

                entity.HasOne(d => d.AccountNavigation)
                    .WithMany(p => p.CostOfGoodsSolds)
                    .HasForeignKey(d => d.Account)
                    .HasConstraintName("FK__cost_of_g__accou__4CA06362");

                entity.HasOne(d => d.Sales)
                    .WithMany(p => p.CostOfGoodsSolds)
                    .HasForeignKey(d => d.SalesId)
                    .HasConstraintName("FK__cost_of_g__sales__4D94879B");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("countries");

                entity.Property(e => e.CountryId).HasColumnName("country_id");

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("country_name");

                entity.Property(e => e.RegionId).HasColumnName("region_id");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Countries)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK__countries__regio__4E88ABD4");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("customers");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("address");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.CountryId).HasColumnName("country_id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("firstName");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("lastName");

                entity.Property(e => e.PhoneNumber).HasColumnName("phone_number");

                entity.Property(e => e.PostalCode).HasColumnName("postal_code");

                entity.Property(e => e.StateProvince)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("state_province");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK__customers__count__4F7CD00D");
            });

            modelBuilder.Entity<DeliveryStatusType>(entity =>
            {
                entity.ToTable("delivery_status_types");

                entity.Property(e => e.DeliveryStatusTypeId).HasColumnName("delivery_status_type_id");

                entity.Property(e => e.DeliveryTypeDescription)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("delivery_type_description");

                entity.Property(e => e.DeliveryTypeName)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("delivery_type_name");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("departments");

                entity.Property(e => e.DepartmentId).HasColumnName("department_id");

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("department_name");

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Departments)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK__departmen__locat__5070F446");
            });

            modelBuilder.Entity<Dependent>(entity =>
            {
                entity.ToTable("dependents");

                entity.Property(e => e.DependentId).HasColumnName("dependent_id");

                entity.Property(e => e.EmployeeId).HasColumnName("employee_id");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("firstName");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("lastName");

                entity.Property(e => e.Relationship)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("relationship");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Dependents)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK__dependent__emplo__5165187F");
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.ToTable("discounts");

                entity.Property(e => e.DiscountId).HasColumnName("discount_id");

                entity.Property(e => e.Account).HasColumnName("account");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Discription)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("discription");

                entity.Property(e => e.SalesId).HasColumnName("sales_id");

                entity.Property(e => e.TransactionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("transaction_date");

                entity.HasOne(d => d.AccountNavigation)
                    .WithMany(p => p.Discounts)
                    .HasForeignKey(d => d.Account)
                    .HasConstraintName("FK__discounts__accou__52593CB8");

                entity.HasOne(d => d.Sales)
                    .WithMany(p => p.Discounts)
                    .HasForeignKey(d => d.SalesId)
                    .HasConstraintName("FK__discounts__sales__534D60F1");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("employees");

                entity.Property(e => e.EmployeeId).HasColumnName("employee_id");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.DepartmentId).HasColumnName("department_id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("firstName");

                entity.Property(e => e.HireDate)
                    .HasColumnType("date")
                    .HasColumnName("hire_date");

                entity.Property(e => e.JobId).HasColumnName("job_id");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("lastName");

                entity.Property(e => e.PhoneNumber).HasColumnName("phone_number");

                entity.Property(e => e.PostalCode).HasColumnName("postal_code");

                entity.Property(e => e.StateProvince)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("state_province");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK__employees__depar__619B8048");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK__employees__job_i__5441852A");
            });

            modelBuilder.Entity<Expense>(entity =>
            {
                entity.HasKey(e => e.ExpensesId)
                    .HasName("PK__expenses__6A5F74676F143877");

                entity.ToTable("expenses");

                entity.Property(e => e.ExpensesId).HasColumnName("expenses_id");

                entity.Property(e => e.Account).HasColumnName("account");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Discription)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("discription");

                entity.Property(e => e.ExpensesTypeId).HasColumnName("expenses_type_id");

                entity.Property(e => e.SalesId).HasColumnName("sales_id");

                entity.Property(e => e.TransactionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("transaction_date");

                entity.HasOne(d => d.AccountNavigation)
                    .WithMany(p => p.Expenses)
                    .HasForeignKey(d => d.Account)
                    .HasConstraintName("FK__expenses__accoun__5535A963");

                entity.HasOne(d => d.ExpensesType)
                    .WithMany(p => p.Expenses)
                    .HasForeignKey(d => d.ExpensesTypeId)
                    .HasConstraintName("FK__expenses__expens__571DF1D5");

                entity.HasOne(d => d.Sales)
                    .WithMany(p => p.Expenses)
                    .HasForeignKey(d => d.SalesId)
                    .HasConstraintName("FK__expenses__sales___5629CD9C");
            });

            modelBuilder.Entity<ExpensesType>(entity =>
            {
                entity.ToTable("expenses_types");

                entity.Property(e => e.ExpensesTypeId).HasColumnName("expenses_type_id");

                entity.Property(e => e.Discription)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("discription");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasKey(e => e.InvoicesId)
                    .HasName("PK__invoices__E7BF16DB98FF5A4B");

                entity.ToTable("invoices");

                entity.Property(e => e.InvoicesId).HasColumnName("invoices_id");

                entity.Property(e => e.InvoicePaidId).HasColumnName("invoice_paid_id");

                entity.Property(e => e.SalesId).HasColumnName("sales_id");

                entity.HasOne(d => d.InvoicePaid)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.InvoicePaidId)
                    .HasConstraintName("FK__invoices__invoic__59063A47");

                entity.HasOne(d => d.Sales)
                    .WithMany(p => p.Invoices)
                    .HasForeignKey(d => d.SalesId)
                    .HasConstraintName("FK__invoices__sales___5812160E");
            });

            modelBuilder.Entity<InvoicePaid>(entity =>
            {
                entity.ToTable("invoice_paid");

                entity.Property(e => e.InvoicePaidId).HasColumnName("invoice_paid_id");

                entity.Property(e => e.InvoiceFinalDate)
                    .HasColumnType("date")
                    .HasColumnName("invoice_finalDate");

                entity.Property(e => e.InvoicePaid1).HasColumnName("Invoice_paid");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("jobs");

                entity.Property(e => e.JobId).HasColumnName("job_id");

                entity.Property(e => e.JobTitle)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("job_title");
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("locations");

                entity.Property(e => e.LocationId).HasColumnName("location_id");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("city");

                entity.Property(e => e.CountryId).HasColumnName("country_id");

                entity.Property(e => e.PostalCode).HasColumnName("postal_code");

                entity.Property(e => e.StateProvince)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("state_province");

                entity.Property(e => e.StreetAddress)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("street_address");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK__locations__count__59FA5E80");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("product_name");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK__products__catego__5AEE82B9");
            });

            modelBuilder.Entity<ProductSale>(entity =>
            {
                entity.HasKey(e => e.Psid);

                entity.ToTable("product_sales");

                entity.Property(e => e.Psid).HasColumnName("psid");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.SalesId).HasColumnName("sales_id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductSales)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_sales_products");

                entity.HasOne(d => d.Sales)
                    .WithMany(p => p.ProductSales)
                    .HasForeignKey(d => d.SalesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_sales");
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.ToTable("regions");

                entity.Property(e => e.RegionId).HasColumnName("region_id");

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("region_name");
            });

            modelBuilder.Entity<RegionProduct>(entity =>
            {
                entity.HasKey(e => e.RegionProductsId)
                    .HasName("PK__region_p__772F3BB7536D2877");

                entity.ToTable("region_products");

                entity.Property(e => e.RegionProductsId).HasColumnName("region_products_id");

                entity.Property(e => e.ProductId).HasColumnName("product_id");

                entity.Property(e => e.RegionId).HasColumnName("region_id");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.RegionProducts)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK__region_pr__produ__5BE2A6F2");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.RegionProducts)
                    .HasForeignKey(d => d.RegionId)
                    .HasConstraintName("FK__region_pr__regio__5CD6CB2B");
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.HasKey(e => e.SalesId)
                    .HasName("PK__sales__995B8585C080C4B4");

                entity.ToTable("sales");

                entity.Property(e => e.SalesId).HasColumnName("sales_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.TransactionDate)
                    .HasColumnType("datetime")
                    .HasColumnName("transaction_date");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Sales)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__sales__customer___5DCAEF64");
            });

            modelBuilder.Entity<SalesDelivered>(entity =>
            {
                entity.ToTable("sales_delivereds");

                entity.Property(e => e.SalesDeliveredId).HasColumnName("sales_delivered_id");

                entity.Property(e => e.DeliveryStatusTypeId).HasColumnName("delivery_status_type_id");

                entity.Property(e => e.SalesId).HasColumnName("sales_id");

                entity.HasOne(d => d.DeliveryStatusType)
                    .WithMany(p => p.SalesDelivereds)
                    .HasForeignKey(d => d.DeliveryStatusTypeId)
                    .HasConstraintName("FK__sales_del__deliv__60A75C0F");

                entity.HasOne(d => d.Sales)
                    .WithMany(p => p.SalesDelivereds)
                    .HasForeignKey(d => d.SalesId)
                    .HasConstraintName("FK__sales_del__sales__5FB337D6");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class ProductSale
    {
        public int Psid { get; set; }
        public int ProductId { get; set; }
        public int SalesId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Sale Sales { get; set; }
    }
}

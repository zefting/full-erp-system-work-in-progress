﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Country
    {
        public Country()
        {
            Customers = new HashSet<Customer>();
            Locations = new HashSet<Location>();
        }

        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int? RegionId { get; set; }

        public virtual Region Region { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
    }
}

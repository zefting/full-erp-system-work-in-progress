﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductSales = new HashSet<ProductSale>();
            RegionProducts = new HashSet<RegionProduct>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int? CategoryId { get; set; }

        public virtual Category Category { get; set; }
        public virtual ICollection<ProductSale> ProductSales { get; set; }
        public virtual ICollection<RegionProduct> RegionProducts { get; set; }
    }
}

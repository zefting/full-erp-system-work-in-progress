﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Invoice
    {
        public int InvoicesId { get; set; }
        public int? SalesId { get; set; }
        public int? InvoicePaidId { get; set; }

        public virtual InvoicePaid InvoicePaid { get; set; }
        public virtual Sale Sales { get; set; }
    }
}

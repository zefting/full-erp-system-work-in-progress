﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Account
    {
        public Account()
        {
            CostOfGoodsSolds = new HashSet<CostOfGoodsSold>();
            Discounts = new HashSet<Discount>();
            Expenses = new HashSet<Expense>();
        }

        public int AccountId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CostOfGoodsSold> CostOfGoodsSolds { get; set; }
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Expense> Expenses { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Region
    {
        public Region()
        {
            Countries = new HashSet<Country>();
            RegionProducts = new HashSet<RegionProduct>();
        }

        public int RegionId { get; set; }
        public string RegionName { get; set; }

        public virtual ICollection<Country> Countries { get; set; }
        public virtual ICollection<RegionProduct> RegionProducts { get; set; }
    }
}

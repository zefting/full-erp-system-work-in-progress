﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Sales = new HashSet<Sale>();
        }

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int? CountryId { get; set; }
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<Sale> Sales { get; set; }
    }
}

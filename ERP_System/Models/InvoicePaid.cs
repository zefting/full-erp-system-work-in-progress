﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class InvoicePaid
    {
        public InvoicePaid()
        {
            Invoices = new HashSet<Invoice>();
        }

        public int InvoicePaidId { get; set; }
        public DateTime? InvoiceFinalDate { get; set; }
        public bool? InvoicePaid1 { get; set; }

        public virtual ICollection<Invoice> Invoices { get; set; }
    }
}

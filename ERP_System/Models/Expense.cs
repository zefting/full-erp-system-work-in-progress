﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Expense
    {
        public int ExpensesId { get; set; }
        public int? Account { get; set; }
        public string Discription { get; set; }
        public double? Amount { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int? SalesId { get; set; }
        public int? ExpensesTypeId { get; set; }

        public virtual Account AccountNavigation { get; set; }
        public virtual ExpensesType ExpensesType { get; set; }
        public virtual Sale Sales { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ERP_System.Models
{
    public partial class Sale
    {
        public Sale()
        {
            CostOfGoodsSolds = new HashSet<CostOfGoodsSold>();
            Discounts = new HashSet<Discount>();
            Expenses = new HashSet<Expense>();
            Invoices = new HashSet<Invoice>();
            ProductSales = new HashSet<ProductSale>();
            SalesDelivereds = new HashSet<SalesDelivered>();
        }

        public int SalesId { get; set; }
        public int? CustomerId { get; set; }
        public DateTime? TransactionDate { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<CostOfGoodsSold> CostOfGoodsSolds { get; set; }
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Expense> Expenses { get; set; }
        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<ProductSale> ProductSales { get; set; }
        public virtual ICollection<SalesDelivered> SalesDelivereds { get; set; }
    }
}

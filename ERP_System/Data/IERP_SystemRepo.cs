﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP_System.Models;
using ERP_System.Dtos;
namespace ERP_System.Data
{
    public interface IERP_SystemRepo
    {
        // Save Changes
        bool SaveChanges();

        // sales
        Sale getSaleById(int sale_id);
        IEnumerable<Sale> GetSales();
        void CreateSale(Sale saleModel);

        //sales delivered
        SalesDelivered getDeliveryStatus(int sales_id);
        IEnumerable<SalesDelivered> GetSalesNotDelivered();
        //Product
        Product GetProductById(int product_id);
        // Expences
        IEnumerable<Expense> getExpencesForSaleById(int sales_id);

        // Employee
        Employee GetEmployeeById(int employee_id);
        IEnumerable<Employee> GetEmployees();
        IEnumerable<Employee> GetEmployeesByDepartment( int department_id);
        void CreateEmployee(Employee employeeModel);
        // customers
        Customer GetCustomerById(int customer_id);
        IEnumerable<Customer> GetCustomers();
        void CreateCustomer(Customer customerModel);
        // invoice status
        Invoice GetInvoiceById(int invoice_id);

        // country
        Country GetCountryById(int country_id);
        IEnumerable<CountryWithRegionDto> GetAllCountryWithRegion();
        IEnumerable<Country> GetAllCountries();
        // region
        Region GetRegionById(int region_id);
        
        //location
        Location GetLocationById(int location_id);
        //departments
        Department GetDepartmentById(int department_id);
        // job
        Job GetJobById(int job_Id);
        //Category
        Category GetCategoryById(int category_id);
        // regionProducts
        void SaveRegionProductsforSale(List<RegionProduct> regionProducts);
        //salesreProducts
        void SaveSaleProducts(List<ProductSale> salesProductsList);
        
    }
}

﻿using ERP_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP_System.Dtos;
namespace ERP_System.Data
{
    public class ERP_SystemRepo : IERP_SystemRepo
    {
        private readonly ERP_systemContext _context;
        public ERP_SystemRepo(ERP_systemContext context)
        {
            _context = context;
        }
        public Customer GetCustomerById(int customer_id)
        {
            return _context.Customers.FirstOrDefault(customer => customer.CustomerId == customer_id);
        }

        public IEnumerable<Customer> GetCustomers()
        {
            return _context.Customers.ToList();
        }
        public void CreateCustomer(Customer customerModel)
        {
            if (customerModel != null)
            {
                throw new ArgumentNullException(nameof(customerModel));
            }
            _context.Customers.Add(customerModel);
        }
        public Product GetProductById(int product_id)
        {
            return _context.Products.FirstOrDefault(product => product.ProductId == product_id);
        }
        public SalesDelivered getDeliveryStatus(int sales_id)
        {
            return _context.SalesDelivereds.FirstOrDefault(status => status.SalesId == sales_id);
        }

        public Employee GetEmployeeById(int employee_id)
        {
            return _context.Employees.FirstOrDefault(employee => employee.EmployeeId == employee_id);
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return _context.Employees.ToList();
        }
        public void CreateEmployee(Employee employeeModel)
        {
            if (employeeModel == null)
            {
                throw new ArgumentNullException(nameof(employeeModel));
            }
            _context.Employees.Add(employeeModel);
        }
        public IEnumerable<Employee> GetEmployeesByDepartment(int department_id)
        {
            return _context.Employees.Where(employee => employee.DepartmentId == department_id).ToList();
        }

        public IEnumerable<Expense> getExpencesForSaleById(int sales_id)
        {
            return _context.Expenses.Where(expense => expense.SalesId == sales_id).ToList();
        }

        public Invoice GetInvoiceById(int invoice_id)
        {
            return _context.Invoices.FirstOrDefault(invoice => invoice.InvoicesId == invoice_id);
        }

        public Sale getSaleById(int sale_id)
        {
            return _context.Sales.FirstOrDefault(sale => sale.SalesId == sale_id);
        }

        public IEnumerable<Sale> GetSales()
        {
            return _context.Sales.ToList();
        }
        public void CreateSale(Sale saleModel)
        {
            if (saleModel != null)
            {
                throw new ArgumentNullException(nameof(saleModel));
            }
            _context.Sales.Add(saleModel);
        }
        public IEnumerable<SalesDelivered> GetSalesNotDelivered()
        {
            return _context.SalesDelivereds.Where(sale => sale.DeliveryStatusTypeId != 2).ToList();
        }

        public Country GetCountryById(int country_id)
        {
            return _context.Countries.FirstOrDefault(country => country.CountryId == country_id);
        }
        public IEnumerable<Country> GetAllCountries()
        {
            return _context.Countries.ToList();
        }

        public Region GetRegionById(int region_id)
        {
            return _context.Regions.FirstOrDefault(region => region.RegionId == region_id);
        }
        public IEnumerable<CountryWithRegionDto> GetAllCountryWithRegion()
        {
            var regions  = _context.Regions.ToList();
            var countries = _context.Countries.ToList();
            List<CountryWithRegionDto> countryWithRegionDtosList = new();
            foreach (var country in countries)
            {
                CountryWithRegionDto countryWithRegionDto = new();
                countryWithRegionDto.CountryId = country.CountryId;
                countryWithRegionDto.CountryName = country.CountryName;
                Region regiont = regions.FirstOrDefault(r => r.RegionId == country.RegionId);

                countryWithRegionDto.RegionName = regiont.RegionName;
                countryWithRegionDtosList.Add(countryWithRegionDto);
            }
            return countryWithRegionDtosList;
        }
        
        public Location GetLocationById(int location_id)
        {
            return _context.Locations.FirstOrDefault(location => location.LocationId == location_id);
        }
        public Job GetJobById(int job_Id)
        {
            return _context.Jobs.FirstOrDefault(job => job.JobId == job_Id);
        }
        public Category GetCategoryById(int category_id)
        {
            return _context.Categories.FirstOrDefault(category => category.CategoryId == category_id);
        }
        public Department GetDepartmentById(int department_id)
        {
            return _context.Departments.FirstOrDefault(department => department.DepartmentId == department_id);
        }

        // regionProducts
        public void SaveRegionProductsforSale(List<RegionProduct> regionProducts)
        {
            if (regionProducts != null)
            {
                throw new ArgumentNullException(nameof(regionProducts));
            }
            _context.RegionProducts.AddRange(regionProducts);
        }

       
        //salesreProducts
        public void SaveSaleProducts(List<ProductSale> salesProductsList)
        {
            if (salesProductsList != null)
            {
                throw new ArgumentNullException(nameof(salesProductsList));
            }
            _context.ProductSales.AddRange(salesProductsList);
        }
        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP_System.Models;
using ERP_System.Dtos;
namespace ERP_System.Data
{
    public interface IMapperService
    {
        public CategoryDto MapToCategoryDto(Category category);
       
        public RegionDto MapToRegionDto(Region region);
        
        public CountryDto MapToCountryDto(Country country);
        
        public ProductDto MapToProductDto(Product product);
        
        public RegionProductDto MapToRegionProductDto(RegionProduct regionProduct);
        
        public Customer customerToDb(CustomerCreateDto customerCreateDto);
        
        public CustomerReadDto MapToCustomerDto(Customer customer);
        
        public LocationDto MapToLocationDto(Location location);
        
        public DepartmentDto MapToDepartmentDto(Department department);
       
        public Sale SaleToDb(SaleCreateDto saleDto);
        
        public SaleDto MapToSalesDto(Sale sale);
       
        public JobDto MapToJobDto(Job job);
       
        public EmployeeDto MapToEmployeeDto(Employee employee);
        
        public Employee employeeToDb(EmployeeDto employeeDto);
        public RegionAndProductListDto MapToregionAndProductListDto(SaleCreateDto saleCreateDto, int saleId);

    }
}

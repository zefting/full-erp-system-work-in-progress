USE [ERP_System]
GO

INSERT INTO [dbo].[regions]
           ([region_name])
     VALUES
           ('EU-West', 'EU-East', 'China', 'Pacific', 'US-East', 'US-West')
GO

USE [ERP_System]
GO

INSERT INTO [dbo].[countries]
           ([country_name]
           ,[region_id])
     VALUES
           ('Denmark',1), ('Germany',1), ('USA', 5)
GO

USE [ERP_System]
GO

INSERT INTO [dbo].[locations]
           ([street_address]
           ,[postal_code]
           ,[city]
           ,[state_province]
           ,[country_id])
     VALUES
           ('First Address',1111,'First City',NULL,4),
		   ('First Address',1111,'2nd City',NULL,5),
		   ('First Address',1111,'3nd City',NULL,6),
		   ('First Address',1111,'4nd City',NULL,4)
GO


USE [ERP_System]
GO

INSERT INTO [dbo].[departments]
           ([department_name]
           ,[location_id])
     VALUES
           ('HR',1),
		   ('Payroll',1),
		   ('Develupment',2),
		   ('Reserch',2),
		   ('Sales',3),
		   ('PR',4)

GO


USE [ERP_System]
GO

INSERT INTO [dbo].[jobs]
           ([job_title])
     VALUES
           ('Key Account Maneger'),
		   ('CEO'),
		   ('RND'),
		   ('Ingeniør'),
		   ('Cook'),
		   ('Driver'),
               ('HR-Rep')
GO


USE [ERP_System]
GO

INSERT INTO [dbo].[employees]
           ([firstName]
           ,[lastName]
           ,[email]
           ,[phone_number]
           ,[hire_date]
           ,[postal_code]
           ,[city]
           ,[state_province]
           ,[job_id]
           ,[department_id])
     VALUES
           ('John'
           ,'Johnson'
           ,'John@Johnson.com'
           ,99999999
           ,'2005-12-20'
           ,1111
           ,'John City'
           ,'John State'
           ,1
           ,5),

		   ('Ariya'
           ,'Hamilton'
           ,'Ariya@Hamilton.com'
           ,99999999
           ,'2011-05-05'
           ,1111
           ,'Ariya City'
           ,'Ariya State'
           ,2
           ,1),

		   ('Samantha'
           ,'Brennan'
           ,'Samantha@Brennan.com'
           ,99999999
           ,'2020-06-04'
           ,1111
           ,'Samantha City'
           ,'Samantha State'
           ,4
           ,3),

		   ('Hadley'
           ,'Woodward'
           ,'Hadley@Woodward.com'
           ,99999999
           ,'2021-08-01'
           ,1111
           ,'Hadley City'
           ,'Hadley State'
           ,1
           ,5),

		   ('Bianca'
           ,'Key'
           ,'John@Johnson.com'
           ,99999999
           ,'2012-01-05'
           ,1111
           ,'John City'
           ,'John State'
           ,1
           ,2),

		   ('Tristan'
           ,'Diaz'
           ,'John@Johnson.com'
           ,99999999
           ,'2019-05-06'
           ,1111
           ,'John City'
           ,'John State'
           ,4
           ,4)
GO


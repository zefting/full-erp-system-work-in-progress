CREATE TABLE [dbo].[jobs]
(
	job_id INT NOT NULL PRIMARY KEY identity,
	job_title varchar(50) NOT NULL
)
GO;

CREATE TABLE [dbo].[regions]
(
	region_id INT NOT NULL PRIMARY KEY identity,
	region_name varchar(50) NOT NULL
)
GO;

CREATE TABLE [dbo].[countries]
(
	country_id INT NOT NULL PRIMARY KEY identity,
	country_name varchar(50) NOT NULL,
	region_id int foreign key references regions(region_id)
)
GO;

CREATE TABLE [dbo].[locations]
(
	location_id INT NOT NULL PRIMARY KEY identity,
	street_address varchar(500) NOT NULL,
	postal_code int NOT NULL,
	city varchar (500) NOT NULL,
	state_province varchar(500),
	country_id int foreign key references countries(country_id) 
	)
GO;

CREATE TABLE [dbo].[departments]
(
	department_id INT NOT NULL PRIMARY KEY identity,
	department_name varchar(50) NOT NULL,
	location_id int foreign key references locations(location_id)
)
GO;
CREATE TABLE [dbo].[employees]
(
	employee_id INT NOT NULL PRIMARY KEY identity,
	firstName varchar(100) NOT NULL,
	lastName varchar(100) NOT NULL,
	email varchar(255) NOT NULL,
	phone_number int NOT NULL,
	hire_date date Not NULL,
	postal_code int NOT NULL,
	city varchar (500) NOT NULL,
	state_province varchar(500),
	job_id int foreign key references jobs(job_id)
)
GO;
CREATE TABLE [dbo].[dependents]
(
	dependent_id INT NOT NULL PRIMARY KEY identity,
	firstName varchar(100) NOT NULL,
	lastName varchar(100) NOT NULL,
	relationship varchar(255) NOT NULL,
	employee_id int foreign key references employees(employee_id)
)
GO;

CREATE TABLE [dbo].[categories]
(
	category_id INT NOT NULL PRIMARY KEY identity,
	name varchar(100) NOT NULL
)
GO;
CREATE TABLE [dbo].[products]
(
	product_id INT NOT NULL PRIMARY KEY identity,
	product_name varchar(100) NOT NULL,
	category_id int foreign key references categories(category_id)
)
GO;

CREATE TABLE [dbo].[region_products]
(
	region_products_id INT NOT NULL PRIMARY KEY identity,
	product_id int foreign key references products(product_id),
	region_id int foreign key references regions(region_id)
)
GO;

CREATE TABLE [dbo].[customers]
(
	customer_id INT NOT NULL PRIMARY KEY identity,
	firstName varchar(100) NOT NULL,
	lastName varchar(100) NOT NULL,
	address varchar(100) NOT NULL,
	country_id int foreign key references countries(country_id),
	email varchar(255) NOT NULL,
	phone_number int NOT NULL,
	postal_code int NOT NULL,
	city varchar (500) NOT NULL,
	state_province varchar(500)
)
GO;

CREATE TABLE [dbo].[sales]
(
	sales_id INT NOT NULL PRIMARY KEY identity,
	customer_id int foreign key references customers(customer_id),
	region_products_id int foreign key references region_products (region_products_id),
	transaction_date datetime
)
GO;
CREATE TABLE [dbo].[accounts]
(
	account_id INT NOT NULL PRIMARY KEY identity,
	description varchar(1000) NOT NULL
)
GO;
CREATE TABLE [dbo].[discounts]
(
	discount_id INT NOT NULL PRIMARY KEY identity,
	account int foreign key references accounts(account_id),
	discription varchar(1000),
	amount float,
	transaction_date datetime,
	sales_id int foreign key references sales(sales_id)
	
)
GO;

CREATE TABLE [dbo].[cost_of_goods_solds]
(
	cost_of_goods_solds_id INT NOT NULL PRIMARY KEY identity,
	account int foreign key references accounts(account_id),
	discription varchar(1000),
	amount float,
	transaction_date datetime,
	sales_id int foreign key references sales(sales_id)
	
)
GO;

CREATE TABLE [dbo].[expenses_types]
(
	expenses_type_id INT NOT NULL PRIMARY KEY identity,
	discription varchar(1000)
)
GO;

CREATE TABLE [dbo].[expenses]
(
	expenses_id INT NOT NULL PRIMARY KEY identity,
	account int foreign key references accounts(account_id),
	discription varchar(1000),
	amount float,
	transaction_date datetime,
	sales_id int foreign key references sales(sales_id),
	expenses_type_id int foreign key references expenses_types(expenses_type_id)
)
GO;

CREATE TABLE [dbo].[delivery_status_types]
(
	delivery_status_type_id INT NOT NULL PRIMARY KEY identity,
	delivery_type_name varchar(1000),
	delivery_type_description varchar(1000)
)
GO;

CREATE TABLE [dbo].[sales_delivereds]
(
	sales_delivered_id INT NOT NULL PRIMARY KEY identity,
	sales_id int foreign key references sales(sales_id),
	delivery_status_type_id int foreign key references delivery_status_types(delivery_status_type_id)
)
GO;

CREATE TABLE [dbo].[invoice_paid]
(
	invoice_paid_id INT NOT NULL PRIMARY KEY identity,
	invoice_finalDate date,
	Invoice_paid bit
)
GO;

CREATE TABLE [dbo].[invoices]
(
	invoices_id INT NOT NULL PRIMARY KEY identity,
	sales_id int foreign key references sales(sales_id),
	invoice_paid_id int foreign key references invoice_paid(invoice_paid_id)
)
GO;

CREATE TABLE [dbo].[sales_region_products]
(
	sales_region_products_id INT NOT NULL PRIMARY KEY identity,
	region_products_id int NOT NULL foreign key references region_products(region_products_id),
	sales_id int NOT NULL foreign key references sales(sales_id) 
)
GO;
